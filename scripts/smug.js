/*
 *
 * Searchbar
 *
 *****************/

$(document).ready(function(){
    let searchBar = localStorage.getItem("searchBar");
    if (searchBar == null) {
        localStorage.setItem("searchBar", 1);
        searchBar = 1;
    }
    if (searchBar == 0) {
        $("#googleBar").toggle();
    }
    $("#settings").toggle();
    $("#bgPopup").toggle();
	$(document).keypress(function(e){
		if (e.which == 13) {
        	let input = $("#searchBar").val();
        	googleSearch(input);
		}
	});
    $("#searchToggle").click(function(){
        let searchBar = localStorage.getItem("searchBar");
        localStorage.setItem("searchBar", searchBar == 1 ? 0 : 1);
        $("#googleBar").toggle();
    })
    $("#button").click(function(){
        let input = $("#searchBar").val();
        googleSearch(input);
    });

    $("#settingsIcon").click(function(){
        $("#settings").slideToggle();
    });
    $("#bgPopupToggle").click(function(){
        $("#bgPopup").fadeToggle();
    });
	$("#closePopup").click(function(){
        $("#bgPopup").fadeToggle();
    });

    /*
     *
     * Music Player
     * Sauce: https://serversideup.net/style-the-html-5-audio-element/
     *****************/
    var activeSong;
    $("#play").click(function(){
        //Sets the active song to the song being played. All other functions depend on this.
        activeSong = document.getElementById("audio");
        //Plays the song defined in the audio tag.
        activeSong.play();
    });
    $("#pause").click(function(){
        activeSong.pause();
    });
    $("#stop").click(function(){
        activeSong.currentTime = 0;
        activeSong.pause();
    });
});

function googleSearch(input) {
    input = encodeURIComponent(input)
    window.location.replace("https://google.com/search?q="+input);
}